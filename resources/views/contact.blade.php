@extends('master');
@section('content')
    <div class="title m-b-md">
        Create Contact
    </div>

    <div class="links">
        <a href="./">Home</a>
        <a href="./companies">Companies</a>
        <a href="./contacts">Contacts</a>
        <a href="./company">Create Company</a>
        <a href="./contact">Create Contact</a>
    </div>
        
    <!-- Entry START-->
    <br><br>
    <form method="POST" action="./contact">
        <div class="form-group flex-center">
        
        <table>
            <tr><td>Company Id: </td><td><input name="company_id" class="form-control"></input></td></tr>
            <tr><td>First Name: </td><td><input name="first_name" class="form-control"></input></td></tr>
            <tr><td>Last Name: </td><td><input name="last_name" class="form-control"></input></td></tr>
            <tr><td>Phone: </td><td><input name="phone" class="form-control"></input></td></tr>
            <tr><td>Email: </td><td><input name="email" class="form-control"></input></td></tr>
        </table>
        </div>
    
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Add Contact</button>
        </div>
    {{ csrf_field() }}
    </form>                
    <!-- Entry END -->
        
@endsection
