@extends('master');
@section('content')
    <div class="title m-b-md">
        Contacts
    </div>

    <div class="links">
        <a href="../../">Home</a>
        <a href="../../companies">Companies</a>
        <a href="../../contacts">Contacts</a>
        <a href="../../company">Create Company</a>
        <a href="../../contact">Create Contact</a>
            
    </div>
    <br><br>
    <h3>{{ $company->name }}</h3>
    <br><br>
    <table>
    <tr><th>ID</th><th>CompanyID</th><th>First</th><th>Last</th><th>Phone</th><th>Email</th></tr>
    @foreach($contacts as $contact)
    <tr>
        <td>{{ $contact->id }}</td>
        <td>{{ $contact->company_id }}</td>
        <td>{{ $contact->first_name }}</td>
        <td>{{ $contact->last_name }}</td>
        <td>{{ $contact->phone }}</td>
        <td>{{ $contact->email }}</td>
        <td>{{ $contact->phone }}</td>
    </tr>
    @endforeach
    </table>

@endsection