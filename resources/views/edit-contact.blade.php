@extends('master');
@section('content')
    <div class="title m-b-md">
        Edit Contact
    </div>

    <div class="links">
        <a href="../../">Home</a>
        <a href="../../companies">Companies</a>
        <a href="../../contacts">Contacts</a>
        <a href="../../company">Create Company</a>
        <a href="../../contact">Create Contact</a>
    </div>
        
    <!-- Entry START-->
    <br><br>
    <form method="POST" action="./{{ $contact->id }}">
        <div class="form-group flex-center">
        
        <table>
            <tr><td>Company Id: </td><td><input name="company_id" value="{{ $contact->company_id }}" class="form-control"></input></td></tr>
            <tr><td>First Name: </td><td><input name="first_name" value="{{ $contact->first_name }}" class="form-control"></input></td></tr>
            <tr><td>Last Name: </td><td><input name="last_name" value="{{ $contact->last_name }}" class="form-control"></input></td></tr>
            <tr><td>Phone: </td><td><input name="phone" value="{{ $contact->phone }}" class="form-control"></input></td></tr>
            <tr><td>Email: </td><td><input name="email" value="{{ $contact->email }}" class="form-control"></input></td></tr>
        </table>
        </div>
    
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Update Contact</button>
        </div>
    {{ csrf_field() }}
    </form>                
    <!-- Entry END -->

@endsection