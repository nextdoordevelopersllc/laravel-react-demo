@extends('master');
@section('content')
    <div class="title m-b-md">
        Games
    </div>

    <div class="links">
        <a href="./">Home</a>
        <a href="./companies">Companies</a>
        <a href="./contacts">Contacts</a>
        <a href="./company">Create Company</a>
        <a href="./contact">Create Contact</a>
    </div>
    <br><br>
    <table>
    <tr><th>ID</th><th>Name</th><th>Address</th><th>City</th><th>State</th><th>Zip</th><th>Phone</th><th></th><th></th><th></th></tr>
    @foreach($games as $game)
    <tr>
        <td>{{ $game->id }}</td>
        <td>{{ $game->name }}</td>
        <td><a href="game/edit/{{ $company->id }}">Edit</a></td>
        <td><a href="game/delete/{{ $company->id }}">Delete</a></td>
    </tr>
    @endforeach
    </table>
@endsection    
