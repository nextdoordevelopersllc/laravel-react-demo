@extends('master');
@section('content')
    <div class="title m-b-md">
        Companies
    </div>

    <div class="links">
        <a href="./">Home</a>
        <a href="./companies">Companies</a>
        <a href="./contacts">Contacts</a>
        <a href="./company">Create Company</a>
        <a href="./contact">Create Contact</a>
    </div>
    <br><br>
    <table>
    <tr><th>ID</th><th>Name</th><th>Address</th><th>City</th><th>State</th><th>Zip</th><th>Phone</th><th></th><th></th><th></th></tr>
    @foreach($companies as $company)
    <tr>
        <td>{{ $company->id }}</td>
        <td>{{ $company->name }}</td>
        <td>{{ $company->address }}</td>
        <td>{{ $company->city }}</td>
        <td>{{ $company->state }}</td>
        <td>{{ $company->zip }}</td>
        <td>{{ $company->phone }}</td>
        <td><a href="contacts/co/{{ $company->id }}">Contact List</a></td>
        <td><a href="company/edit/{{ $company->id }}">Edit</a></td>
        <td><a href="company/delete/{{ $company->id }}">Delete</a></td>
    </tr>
    @endforeach
    </table>
@endsection
