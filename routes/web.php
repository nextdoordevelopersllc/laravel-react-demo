<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $data = array(
        'services' => ['Web Design', 'Artwork', 'Development', 'Marketing']
    );
    return view('welcome')->with($data);
    //return view('welcome');
});

//pure html
Route::get('/test', function () {
    return "<h1>Hello World</h1>";
});

//handle a few params
Route::get("/users/{id}/{name}", function($id, $name){
    return 'this is user ' . $name . " with ID: " . $id;    
});

//Contacts (use controllers)
Route::get('/contacts', 'ContactsController@index');
Route::get('/contacts/co/{cid}', 'ContactsController@index_company');
Route::get('/contact', 'ContactsController@create');
Route::post('/contact', 'ContactsController@store');
Route::get('/contact/edit/{id}', 'ContactsController@edit');
Route::post('/contact/edit/{id}', 'ContactsController@update');
Route::get('/contact/delete/{id}', 'ContactsController@destroy');

//Companies
Route::get('/companies', 'CompaniesController@index');
Route::get('/company', 'CompaniesController@create');
Route::post('/company', 'CompaniesController@store');
Route::get('/company/edit/{id}', 'CompaniesController@edit');
Route::post('/company/edit/{id}', 'CompaniesController@update');
Route::get('/company/delete/{id}', 'CompaniesController@destroy');


//Companies
Route::get('/games', 'GamesController@index');
